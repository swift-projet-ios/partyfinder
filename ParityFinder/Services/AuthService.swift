//
//  AuthService.swift
//  SeminaireFirebase
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import Foundation
import Firebase
import FirebaseAuth

class AuthService{
    
    func createUser(email: String, password: String, completion: @escaping (Result<AuthDataResult,Error>) -> Void){
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            if let error = error {
                print("❤️ Error on create user : \(error) ❤️")
                completion(.failure(error))
            } else if let authDataResult = authDataResult {
                completion(.success(authDataResult))
            }
        }
    }
    
    func signIn(email : String, password: String, completion: @escaping (Result<AuthDataResult,Error>) -> Void){
        Auth.auth().signIn(withEmail: email, password: password) { (authDataResult, error) in
            if let error = error {
                print("❤️Error on SignIn user : \(error) ❤️")
                completion(.failure(error))
            } else if let authDataResult = authDataResult {
                completion(.success(authDataResult))
            }
        }
    }
    
}
