//
//  FirestoreService.swift
//  SeminaireFirebase
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class FirestoreService {
    
    let db = Firestore.firestore()
    
    func addData(collectionName: String, data : [String : Any]){
        db.collection(collectionName).addDocument(data: data) { (error) in
            if let error = error {
                print("❤️ error add data on \(collectionName) => \(error) ❤️")
            }else{
                print("💚 Succes add data on \(collectionName) 💚")
            }
        }
        
    }
    
    func updateData(collectionName: String, documentPath : String,fields : [AnyHashable :  Any])  {
        db.collection(collectionName).document(documentPath).updateData(fields) { (error) in
            if let error = error {
                print("❤️ error update data on \(collectionName)  with document \(documentPath)=> \(error) ❤️")
            }else{
                print("💚 Succes update data on \(collectionName)  with document \(documentPath) 💚")
            }
        }
        
    }
    
    func deleteData(collectionName: String, documentPath : String ){
        db.collection(collectionName).document(documentPath).delete{
            error in
            if let error = error {
                print("❤️ error delete data on \(collectionName)  with document \(documentPath)=> \(error) ❤️")
            }else{
                print("💚 Succes delete data on \(collectionName)  with document \(documentPath) 💚")
            }
        
        }
        
    }
    
    func getData(collectionName: String, documentPath : String){
        db.collection(collectionName).getDocuments() {
            (querySnapshot, error) in
            if let error = error {
                print("❤️ error in listing data on \(collectionName) ❤️")
            }else{
                for document in querySnapshot!.documents{
                    print("💚 document : \(document)")
                }
            }
            
        }
    }
}
