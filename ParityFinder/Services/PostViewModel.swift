//
//  PostViewModel.swift
//  ParityFinder
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import Foundation
import FirebaseFirestore

class PostViewModel : ObservableObject {
    @Published var post = [Post]()
    
    var db = Firestore.firestore()
    
    func addPost(post: Post){
        do{
            let _ = try db.collection("post").addDocument(from: post)
        }
        catch{
            print(error)
        }
    }
    
    func fetchData(){
        db.collection("post").addSnapshotListener{ (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("🧡 no document error : \(error)")
                return
            }
            print("🧡WE HAVE document")
            self.post = documents.compactMap({ (QueryDocumentSnapshot) -> Post? in
                return try? QueryDocumentSnapshot.data(as: Post.self)
                
            })
        }
    }
}
