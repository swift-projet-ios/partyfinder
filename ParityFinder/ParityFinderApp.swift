//
//  ParityFinderApp.swift
//  ParityFinder
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import SwiftUI
import Firebase


class AppDelegate: NSObject, UIApplicationDelegate {
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
       FirebaseApp.configure()
       return true
   }
}
@main
struct PartyFinderApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    var body: some Scene {
        WindowGroup {
            SignInView()
        }
    }
}

