//
//  MainTabView.swift
//  ParityFinder
//
//  Created by Mathis Kazek on 04/05/2021.
//

import SwiftUI

extension UIColor {
    static let offWhite = UIColor(red: 225 / 255, green: 225 / 255, blue: 235 / 255,alpha: 1.0)
}

struct MainTabView: View {
    init() {
        UINavigationBar.appearance().tintColor = UIColor.offWhite
        UINavigationBar.appearance().titleTextAttributes = [
            .font : UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : UIColor.black
        ]
    }
    
    @State private var user = UserDefaults.standard.data(forKey: "user")
    
    var body: some View {
        
        if user != nil {
            TabView {
                NavigationView {
                    ZStack{
                        Color.offWhite.edgesIgnoringSafeArea(.all)
                        FeedGlobalView()
                    }
                }
                .tabItem {
                    Image(systemName: "house")
                    Text("Home")
                }
            AccountsView()
                .tabItem {
                    Image(systemName: "person.crop.circle.fill")
                    Text("Mon Compte")
                }
            }
        } else {
            SignUpView()
        }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
