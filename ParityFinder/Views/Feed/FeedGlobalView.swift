//
//  FeedGlobalView.swift
//  ParityFinder
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import SwiftUI

extension Color {
    static let offWhite = Color(red: 225 / 255, green: 225 / 255, blue: 235 / 255)
}

struct FeedGlobalView: View {
    
    
    @ObservedObject private var viewModel = PostViewModel()
    var body: some View {
        
        
        ScrollView {
            
            ForEach(viewModel.post) { post in
                ZStack(){
                    Color.offWhite
                    
                    FeedItemView(title: post.title, desc: post.desc, firstName: post.firstName, lastName: post.lastName).padding()
                }
            }
        }.navigationBarItems(trailing: HStack { NavigationLink(
            destination: FeedPostView(),
            label: {
                Image(systemName: "plus.square.fill")
                    .font(.largeTitle)
            }).foregroundColor(.blue)
        })
        .navigationBarTitle(Text("Your Feed"), displayMode: .inline)
        .onAppear(){
            self.viewModel.fetchData()
        }
        
        
    }
    
    
    
}

struct FeedGlobalView_Previews: PreviewProvider {
    static var previews: some View {
        FeedGlobalView()
    }
}
