//
//  FeedItemView.swift
//  ParityFinder
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import SwiftUI

struct FeedItemView: View {
    var title : String
    var desc : String
    var firstName : String
    var lastName : String
    
    var body: some View {
        
        VStack(spacing: 0){
            HStack(alignment: .top){
                Image("profile")
                    .resizable()
                    .frame(width: 64.0, height: 64.0)
                    .clipShape(Circle())
                    .shadow(radius: 5)
                    .overlay(Circle().stroke(Color.offWhite,lineWidth: 3))
                Spacer()
                VStack{
                    Text("\(firstName) \(lastName)").font(.subheadline)
                    Text("date : 10/12/21").font(.subheadline).italic()
                }
            }
            .padding(.bottom )
            Text(title).fontWeight(.medium).frame(alignment: .leading)
            Text(desc).padding(.top).frame(alignment: .leading)
        }
        .padding()
        .background(Color.white)
        .cornerRadius(25)
        .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
        .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
        
    }
}

struct FeedItemView_Previews: PreviewProvider {
    static var previews: some View {
        FeedItemView(title: "", desc: "" ,firstName: "", lastName: "")
    }
}
