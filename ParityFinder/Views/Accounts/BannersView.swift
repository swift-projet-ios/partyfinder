//
//  BannierView.swift
//  ParityFinder
//
//  Created by Paul on 05/05/2021.
//

import SwiftUI

struct BanniersView: View {
        var body: some View {
            Image("bannier")
                .resizable()
    }
}

struct BanniersView_Previews: PreviewProvider {
    static var previews: some View {
        BanniersView()
    }
}
