//
//  CircleImage.swift
//  ParityFinder
//
//  Created by Paul on 05/05/2021.
//

import SwiftUI

struct CirclesImage: View {
    var body: some View {
        Image("profile")
            .resizable()
            .frame(width: 140.0, height: 140.0)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.gray, lineWidth: 4))
            .shadow(radius: 7)
    }
}

struct CirclesImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage()
    }
}
