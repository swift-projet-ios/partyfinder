//
//  AccountView.swift
//  ParityFinder
//
//  Created by Paul on 04/05/2021.
//

import SwiftUI

struct AccountsView: View {
    
var body: some View {

   VStack {
    
    NavigationView {
        VStack{
            VStack{
                BanniersView()
                    .ignoresSafeArea(edges: .top)
                    .frame(height: 180)
                CirclesImage()
                    .offset(y: -80)
                    .padding(.bottom, -80)
                
                List {
                    
                Text("Mes identifiants")
                Text("Changer la bannière")
                Text("Changer la photo de profil")
                Text("Mes événement")
                Text("Deconnexion").foregroundColor(.red)
                 
                }
            }
        }
        .toolbar {
            Button(action: {
                print("Background Color")
            }, label: {
                Text("Paramètres")
                    .background(Color.green)
            })
        }
    }.accentColor(.white)
    Spacer()
    }
  }
}


struct AccountsView_Previews: PreviewProvider {
    static var previews: some View {
        AccountsView()
    }
}
