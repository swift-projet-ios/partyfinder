//
//  FeedPostView.swift
//  ParityFinder
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import SwiftUI

struct FeedPostView: View {
    @State var title : String = ""
    @State var text : String = ""
    @ObservedObject private var viewModel = PostViewModel()
    var body: some View {
        ZStack{
            Color.offWhite.edgesIgnoringSafeArea(.all)
            VStack{
                VStack{
                    TextField("Titre", text: $title).padding()
                    TextField("Descriptions",text : $text).padding()
                    
                    
                }
                .background(Color.white)
                .cornerRadius(25)
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .padding()
                
                
                HStack{
                    Button(action: {
                        self.addPost()
                    }, label: {
                        Text("Publier")
                    })
                    .padding()
                    .padding(.horizontal, 20)
                    .foregroundColor(.white)
                    .font(Font.body.bold())
                }
                .background(Capsule().fill(Color.green))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .padding(.trailing, 10)
            }
        }
    }
    
    
    func addPost(){
        var post = Post(title: self.title, firstName: "test", lastName: "test2", desc: self.text)
        viewModel.addPost(post: post)
    }
    
}

struct FeedPostView_Previews: PreviewProvider {
    static var previews: some View {
        FeedPostView()
    }
}
