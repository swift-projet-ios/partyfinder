//
//  TextFieldView.swift
//  ParityFinder
//
//  Created by Mathis Kazek on 04/05/2021.
//

import SwiftUI

struct CustomTextField: View {
    
    var placeholder: Text
    @Binding var text: String
    var isSecure: Bool
    var editingChanged: (Bool)->() = { _ in }
    var commit: ()->() = { }
    
    var body: some View {
        ZStack(alignment: .leading) {
            if text.isEmpty { placeholder }
            if isSecure {
                SecureField("", text: $text, onCommit: commit)
            } else {
                TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit)
            }
        }
    }
}
