//
//  BannierView.swift
//  ParityFinder
//
//  Created by Paul on 05/05/2021.
//

import SwiftUI

struct BannierView: View {
        var body: some View {
            Image("bannier")
                .resizable()
    }
}

struct BannierView_Previews: PreviewProvider {
    static var previews: some View {
        BannierView()
    }
}
