//
//  AccountView.swift
//  ParityFinder
//
//  Created by Paul on 04/05/2021.
//

import SwiftUI

struct AccountView: View {
    @State private var showingSheet = false
    
var body: some View {

   VStack {
    
    NavigationView {
        VStack{
            VStack{
                BannierView()
                    .ignoresSafeArea(edges: .top)
                    .frame(height: 180)
                
                CircleImage()
                    .offset(y: -80)
                    .padding(.bottom, -80)
                
                List {
               
                Button(action: {
                    print("Mes événements")
                    //Ajouter la route pour retourner sur la page principal
                }, label: {
                    Text("Mes événement")
                })
                    
                Button("Modifier mes identifiants") {
                    showingSheet.toggle()
                }
                .sheet(isPresented: $showingSheet) {
                    SheetView()
                }
                    
                Button(action: {
                    print("Changer la bannière")
                }, label: {
                    Text("Changer la bannière")
                })

                Button(action: {
                    print("Changer la bannière")
                }, label: {
                    Text("Changer la photo de profil")
                })
                    
                Button(action: {
                    print("Déconnexion")
                }, label: {
                    Text("Déconnexion")
                }).foregroundColor(.red)
                 
                }
            }
        }
        .toolbar {
            Button(action: {
                print("Close sheet")
            }, label: {
                Text("Paramètres")
            })
        }
    }.accentColor(.white)
    Spacer()
    }
  }
}


struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
