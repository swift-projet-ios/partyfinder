//
//  SheetView.swift
//  ParityFinder
//
//  Created by Paul on 07/05/2021.
//

import SwiftUI

struct SheetView: View {
@Environment(\.presentationMode) var presentationMode
    @State private var firstname = ""
    @State private var lastname = ""
    @State private var email = ""
    @State private var password = ""
    @State private var repeatPassword = ""
   
    var body: some View {

       VStack {
        
        NavigationView {
            VStack{
                VStack{
                 
                    Form {
                        TextField("Saisir Prénom", text: self.$firstname)
                        TextField("Saisir nom", text: self.$lastname)
                        TextField("Saisir votre email", text: self.$email)
                        TextField("Saisir votre mot de passe", text: self.$password)
                        TextField("Confirmer votre mot de passe", text: self.$repeatPassword)
                    }.navigationBarTitle("Modifier identifiants")
                    
                    Button("Sauvegarder mes modifications") {
                        // enregistrer les modifications
                    }.foregroundColor(.green)
                }
            }
            .toolbar {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "xmark")
                })
            }
        }.accentColor(.black)
        Spacer()
        }
      }
}

struct SheetView_Previews: PreviewProvider {
    static var previews: some View {
        SheetView()
    }
}
