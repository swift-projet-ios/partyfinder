//
//  SignUpView.swift
//  ParityFinder
//
//  Created by Mathis Kazek on 04/05/2021.
//

import SwiftUI

struct SignUpView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var email: String = ""
    @State var password: String = ""
    @State var repeatedPassword: String = ""
    @State var infoMessage: String = ""
    
    @State var firstNameColor = Color.white
    @State var lastNameColor = Color.white
    @State var emailColor = Color.white
    @State var passwordColor = Color.white
    @State var repeatedPasswordColor = Color.white
    
    @State var firstNameAttempts: Int = 0
    @State var lastNameAttempts: Int = 0
    @State var emailAttempts: Int = 0
    @State var passwordAttempts: Int = 0
    @State var repeatedPasswordAttempts: Int = 0
    
    @State private var showInfo = false
    
    let authService = AuthService()
    
    var body: some View {
        
        ZStack(alignment: .topLeading) {
            Color.white.edgesIgnoringSafeArea(.all)
            VStack {
                Text("S'inscrire")
                    .padding()
                    .font(.system(size: 40))
                    .foregroundColor(Color(.darkGray))
                    .padding(.bottom, 20)
                if showInfo {
                    Text(infoMessage)
                        .padding()
                        .foregroundColor(.red)
                        .font(.system(size: 20))
                }
                HStack {
                    Image(systemName: "person").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Nom").foregroundColor(.gray), text: $lastName, isSecure: false)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(lastNameColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(lastNameAttempts)))
                .padding(.bottom, 20)
                HStack {
                    Image(systemName: "person").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Prénom").foregroundColor(.gray), text: $firstName, isSecure: false)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(firstNameColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(firstNameAttempts)))
                .padding(.bottom, 20)
                HStack {
                    Image(systemName: "envelope").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Email").foregroundColor(.gray), text: $email, isSecure: false)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(emailColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(emailAttempts)))
                .padding(.bottom, 20)
                HStack {
                    Image(systemName: "lock").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Mot de passe").foregroundColor(.gray), text: $password, isSecure: true)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(passwordColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(passwordAttempts)))
                .padding(.bottom, 20)
                HStack {
                    Image(systemName: "lock").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Confirmation du mot de passe").foregroundColor(.gray), text: $repeatedPassword, isSecure: true)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(repeatedPasswordColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(repeatedPasswordAttempts)))
                .padding(.bottom, 20)
                
                HStack {
                    HStack {
                        Button(action: {
                            signUp()
                        }, label: {
                            Text("S'inscrire")
                        })
                        .padding()
                        .padding(.horizontal, 25)
                        .foregroundColor(.white)
                        .font(Font.body.bold())
                    }
                    .background(Capsule().fill(Color.green))
                    .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                    .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                    .padding(.trailing, 10)
                    HStack {
                        Button("Retour") {
                            presentationMode.wrappedValue.dismiss()
                        }
                        .padding()
                        .padding(.horizontal, 25)
                        .foregroundColor(.white)
                        .font(Font.body.bold())
                    }
                    .background(Capsule().fill(Color.blue))
                    .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                    .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                }
            }
            .padding()
        }
    }
    
    func signUp() {
        showInfo = true
        firstNameColor = Color.white
        lastNameColor = Color.white
        emailColor = Color.white
        passwordColor = Color.white
        repeatedPasswordColor = Color.white
        if (!self.firstName.isEmpty && !self.lastName.isEmpty && !self.email.isEmpty && !self.password.isEmpty && !self.repeatedPassword.isEmpty) {
            if self.password == self.repeatedPassword {
                authService.createUser(email: self.email, password: self.password) { result in
                    switch result {
                    case .success(let authDataResult):
                        print("User created !")
                        presentationMode.wrappedValue.dismiss()
                    case .failure(let error):
                        print("Created user error")
                        if error.localizedDescription == "The password must be 6 characters long or more." {
                            infoMessage = "Le mot de passe doit faire 6 caractères ou plus !"
                            withAnimation(.default) {
                                self.passwordAttempts += 1
                                self.repeatedPasswordAttempts += 1
                            }
                            passwordColor = Color.red
                            repeatedPasswordColor = Color.red
                        } else if error.localizedDescription == "The email address is already in use by another account." {
                            infoMessage = "Ce compte est déjà existant !"
                            withAnimation(.default) {
                                self.emailAttempts += 1
                            }
                            emailColor = Color.red
                        } else if error.localizedDescription == "The email address is badly formatted." {
                            infoMessage = "L'adresse mail est mal renseignée !"
                            withAnimation(.default) {
                                self.emailAttempts += 1
                            }
                            emailColor = Color.red
                        } else {
                            infoMessage = "Une erreur est survenu. Veuillez contacter le support !"
                        }
                    }
                }
            } else {
                infoMessage = "Les mots de passe sont différents"
                withAnimation(.default) {
                    self.passwordAttempts += 1
                    self.repeatedPasswordAttempts += 1
                }
                passwordColor = Color.red
                repeatedPasswordColor = Color.red
            }
        } else {
            infoMessage = "Veuillez remplir tous les champs"
            withAnimation(.default) {
                self.firstNameAttempts += 1
                self.lastNameAttempts += 1
                self.emailAttempts += 1
                self.passwordAttempts += 1
                self.repeatedPasswordAttempts += 1
            }
            firstNameColor = Color.red
            lastNameColor = Color.red
            emailColor = Color.red
            passwordColor = Color.red
            repeatedPasswordColor = Color.red
        }
    }
}

struct SignInView: View {
    
    @State private var showingSheet = false
    @State var email: String = ""
    @State var password: String = ""
    @State var infoMessage: String = ""
    
    @State private var showInfo = false
    
    @State var emailColor = Color.white
    @State var passwordColor = Color.white
    
    @State var emailAttempts: Int = 0
    @State var passwordAttempts: Int = 0
    
    let authService = AuthService()
    
    var body: some View {
        
        ZStack(alignment: .topLeading) {
            Color.white.edgesIgnoringSafeArea(.all)
            VStack {
                Text("Connexion")
                    .padding()
                    .font(.system(size: 40))
                    .foregroundColor(Color(.darkGray))
                    .padding(.bottom, 110)
                if showInfo {
                    Text(infoMessage)
                        .padding()
                        .foregroundColor(.red)
                        .font(.system(size: 20))
                }
                HStack {
                    Image(systemName: "person").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Email").foregroundColor(.gray), text: $email, isSecure: false)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(emailColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(emailAttempts)))
                .padding(.bottom, 40)
                HStack {
                    Image(systemName: "lock").foregroundColor(.gray)
                    CustomTextField(placeholder: Text("Mot de passe").foregroundColor(.gray), text: $password, isSecure: true)
                        .foregroundColor(Color(.darkGray))
                }
                .padding()
                .overlay(Capsule().stroke(passwordColor))
                .background(Capsule().fill(Color.white))
                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                .modifier(Shake(animatableData: CGFloat(passwordAttempts)))
                .padding(.bottom, 40)
                HStack {
                    HStack {
                        Button(action: {
                            self.signIn()
                        }, label: {
                            Text("Se connecter")
                        })
                        .padding()
                        .padding(.horizontal, 20)
                        .foregroundColor(.white)
                        .font(Font.body.bold())
                    }
                    .background(Capsule().fill(Color.green))
                    .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                    .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                    .padding(.trailing, 10)
                    HStack {
                        Button("S'inscrire") {
                            showingSheet.toggle()
                        }
                        .sheet(isPresented: $showingSheet) {
                            SignUpView()
                        }
                        .padding()
                        .padding(.horizontal, 20)
                        .foregroundColor(.white)
                        .font(Font.body.bold())
                    }
                    .background(Capsule().fill(Color.blue))
                    .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                    .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                }
            }
            .padding()
        }
    }
    
    func signIn() {
        showInfo = true
        emailColor = Color.white
        passwordColor = Color.white
        if self.email != "" && self.password != "" {
            authService.signIn(email: self.email, password: self.password) { result in
                switch result {
                case .success(let authDataResult):
                    print("User connected !")
                    infoMessage = "Utilisateur connecté !"
                    UserDefaults.standard.set(Auth.auth().currentUser, forKey: "user")
                case .failure(let error):
                    print("Connection user error")
                    if error.localizedDescription == "The email address is badly formatted." {
                        infoMessage = "L'adresse mail est mal renseignée !"
                        withAnimation(.default) {
                            self.emailAttempts += 1
                        }
                        emailColor = Color.red
                    } else if error.localizedDescription == "There is no user record corresponding to this identifier. The user may have been deleted." {
                        infoMessage = "Aucun utilisateur n'est lié à cette adresse mail !"
                        withAnimation(.default) {
                            self.emailAttempts += 1
                        }
                        emailColor = Color.red
                    } else if error.localizedDescription == "The password is invalid or the user does not have a password." {
                        infoMessage = "Le mot de passe est incorrect ou l'identifiant est invalide !"
                        withAnimation(.default) {
                            self.emailAttempts += 1
                            self.passwordAttempts += 1
                        }
                        emailColor = Color.red
                        passwordColor = Color.red
                    } else {
                        infoMessage = "Une erreur est survenue lors de la connexion, veuillez contacter le support !"
                    }
                }
            }
        } else {
            infoMessage = "Veuillez remplir tous les champs"
            withAnimation(.default) {
                self.emailAttempts += 1
                self.passwordAttempts += 1
            }
            emailColor = Color.red
            passwordColor = Color.red
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
