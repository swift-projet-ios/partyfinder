//
//  Post.swift
//  ParityFinder
//
//  Created by Antoine Heinrich on 04/05/2021.
//

import Foundation
import FirebaseFirestoreSwift

struct Post : Codable, Identifiable {
    @DocumentID var id: String? = UUID().uuidString
    var title : String
    var firstName : String
    var lastName : String
    var desc : String
    
    enum CodingKeys: String, CodingKey {
        case title
        case firstName
        case lastName
        case desc
    }
}
